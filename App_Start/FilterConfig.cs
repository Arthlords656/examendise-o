﻿using System.Web;
using System.Web.Mvc;

namespace Sistema_de_Control_La_Sibarita
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
