﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sistema_de_Control_La_Sibarita.Startup))]
namespace Sistema_de_Control_La_Sibarita
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
