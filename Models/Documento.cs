//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sistema_de_Control_La_Sibarita.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Documento
    {
        public int IdDoc { get; set; }
        public string ObsDoc { get; set; }
        public string DirDoc { get; set; }
        public int IdReg { get; set; }
    
        public virtual Registro Registro { get; set; }
    }
}
