/*
Created		05/02/2020
Modified		05/02/2020
Project		
Model		
Company		
Author		
Version		
Database		MS SQL 7 
*/


Create table [Usuario] (
	[IdUsu] Integer NOT NULL,
	[CiUsu] Char(15) NULL,
	[NomUsu] Varchar(150) NULL,
	[DirUsu] Varchar(100) NULL,
	[TelUsu] Integer NULL,
	[RolUsu] Varchar(20) NULL,
	[CueUsu] Char(10) NULL,
	[PasUsu] Varbinary(50) NULL,
	[EstUsu] Bit NULL,
Primary Key  ([IdUsu])
) 
go

Create table [Registro] (
	[IdReg] Integer NOT NULL,
	[FeReg] Datetime NULL,
	[ObsReg] Varchar(150) NULL,
	[IdUsu] Integer NOT NULL,
Primary Key  ([IdReg])
) 
go

Create table [Auditoria] (
	[IdAud] Integer NOT NULL,
	[FecAud] Datetime NULL,
	[ObsAud] Varchar(150) NULL,
	[DocAud] Varchar(100) NULL,
	[IdUsu] Integer NOT NULL,
Primary Key  ([IdAud])
) 
go

Create table [Documento] (
	[IdDoc] Integer NOT NULL,
	[ObsDoc] Varchar(150) NULL,
	[DirDoc] Varchar(100) NULL,
	[IdReg] Integer NOT NULL,
Primary Key  ([IdDoc])
) 
go


Alter table [Registro] add  foreign key([IdUsu]) references [Usuario] ([IdUsu]) 
go
Alter table [Auditoria] add  foreign key([IdUsu]) references [Usuario] ([IdUsu]) 
go
Alter table [Documento] add  foreign key([IdReg]) references [Registro] ([IdReg]) 
go


Set quoted_identifier on
go

Set quoted_identifier off
go


